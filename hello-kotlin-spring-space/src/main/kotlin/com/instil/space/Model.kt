package com.instil.space

class Name(var firstName: String,
           var lastName: String) {
    override fun toString() = "$firstName $lastName"
}

class Profile(var id: String,
              var username: String,
              var name: Name) {
    override fun toString() = "$id is $name with username $username"
}

class AllProfilesResponse(var next: String,
                          var totalCount: String,
                          var data: List<Profile>)
