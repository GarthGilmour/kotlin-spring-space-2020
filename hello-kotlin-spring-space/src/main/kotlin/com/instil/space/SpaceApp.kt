package com.instil.space

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.web.reactive.function.client.WebClient

import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@SpringBootApplication
class SpaceApp {
    @Bean
    fun console(webClient: WebClient) = CommandLineRunner {

        val header = Mono.just("Details of all the profiles:")
        val profiles = retrieveSpaceProfiles(webClient).map { "\t $it" }
        val footer = Mono.just("All done - hit return to exit")

        val values = header
                .concatWith(profiles)
                .concatWith(footer)

        values.subscribe(::println)

        //Because this is a console app we need to keep the
        // main thread alive. Otherwise Spring would exit.
        readLine()
    }
}

fun retrieveSpaceProfiles(webClient: WebClient) = webClient
        .get()
        .uri("/team-directory/profiles")
        .retrieve()
        .bodyToFlux(AllProfilesResponse::class.java)
        .flatMap { Flux.fromIterable(it.data) }

fun main(args: Array<String>) {
    runApplication<SpaceApp>(*args)
}
