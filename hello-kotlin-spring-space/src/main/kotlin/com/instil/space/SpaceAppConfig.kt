package com.instil.space

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.oauth2.client.AuthorizedClientServiceReactiveOAuth2AuthorizedClientManager
import org.springframework.security.oauth2.client.InMemoryReactiveOAuth2AuthorizedClientService
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository
import org.springframework.security.oauth2.client.web.reactive.function.client.ServerOAuth2AuthorizedClientExchangeFilterFunction
import org.springframework.web.reactive.function.client.WebClient

@Configuration
class SpaceAppConfig {
    @Bean
    fun oauthWebClient(clientRegistrations: ReactiveClientRegistrationRepository,
                       @Value("\${space.api.url}") baseUrl: String): WebClient {

        val clientService = InMemoryReactiveOAuth2AuthorizedClientService(clientRegistrations)
        val manager = AuthorizedClientServiceReactiveOAuth2AuthorizedClientManager(clientRegistrations, clientService)
        val oauth = ServerOAuth2AuthorizedClientExchangeFilterFunction(manager)
        oauth.setDefaultClientRegistrationId("HELLO")

        return WebClient.builder()
                .filter(oauth)
                .baseUrl(baseUrl)
                .build()
    }
}
